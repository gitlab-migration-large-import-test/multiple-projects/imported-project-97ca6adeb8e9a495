# frozen_string_literal: true

require 'yaml'

module Gitlab
  module QA
    module Scenario
      module Test
        module Integration
          class OAuth < Scenario::Template
            attr_reader :gitlab_name

            def initialize
              @gitlab_name = 'gitlab'
            end

            def perform(release, *rspec_args)
              Runtime::Env.require_oauth_environment!

              release = Release.new(release)

              Component::Gitlab.perform do |gitlab|
                gitlab.release = release
                gitlab.network = 'test'
                gitlab.name = gitlab_name
                gitlab.tls = true

                gitlab.omnibus_configuration << gitlab_omnibus_configuration(gitlab.address)

                gitlab.instance do
                  Runtime::Logger.info('Running OAuth specs!')

                  Component::Specs.perform do |specs|
                    rspec_args << '--' unless rspec_args.include?('--')
                    rspec_args << %w[--tag oauth]
                    specs.suite = 'Test::Instance::All'
                    specs.release = release
                    specs.network = gitlab.network
                    specs.args = [gitlab.address, *rspec_args]
                  end
                end
              end
            end

            private

            def gitlab_omnibus_configuration(gitlab_address)
              <<~OMNIBUS
                gitlab_rails['omniauth_enabled'] = true;
                gitlab_rails['omniauth_allow_single_sign_on'] = ['github', 'facebook'];
                gitlab_rails['omniauth_block_auto_created_users'] = false;
                gitlab_rails['omniauth_providers'] = [
                  {
                    name: 'github',
                    app_id: '$QA_GITHUB_OAUTH_APP_ID',
                    app_secret: '$QA_GITHUB_OAUTH_APP_SECRET',
                    url: 'https://github.com/',
                    verify_ssl: false,
                    args: { scope: 'user:email' }
                  },
                  {
                    name: 'facebook',
                    app_id: '$QA_FACEBOOK_OAUTH_APP_ID',
                    app_secret: '$QA_FACEBOOK_OAUTH_APP_SECRET',
                    verify_ssl: false
                  }
                ];
                letsencrypt['enable'] = false;
                external_url '#{gitlab_address}';
              OMNIBUS
            end
          end
        end
      end
    end
  end
end
